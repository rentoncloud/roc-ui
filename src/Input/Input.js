import React, { forwardRef } from "react";
import { Form, InputGroup as IGroup, Dropdown } from "react-bootstrap";
// import Dropdown from "./Dropdown.js";
// import RangePicker from "./RangePicker.js";

export const InputField = forwardRef(
  ({ name, label, sublabel, type, ...props }, ref) => {
    let r = (Math.random() + 1).toString(36).substring(7);
    const id = props.id ?? r;
    return (
      <Form.Group className="form-group" controlId={id ?? ""}>
        {label ? <Form.Label>{label}</Form.Label> : null}
        <Form.Control type={type} {...props} ref={ref} name={name} />
        {sublabel ? (
          <Form.Text className="text-light">{sublabel}</Form.Text>
        ) : null}
      </Form.Group>
    );
  }
);

export const Text = forwardRef((props, ref) => {
  return <InputField type="text" {...props} ref={ref} />;
});
export const Email = forwardRef((props, ref) => {
  return <InputField type="email" {...props} ref={ref} />;
});
export const Password = forwardRef((props, ref) => {
  return <InputField type="password" {...props} ref={ref} />;
});
export const Number = forwardRef((props, ref) => {
  return <InputField type="number" {...props} ref={ref} />;
});

export const TextArea = forwardRef((props, ref) => {
  return <InputField as="textarea" {...props} ref={ref} />;
});

TextArea.defaultProps = { rows: 3 };

export const InputGroup = forwardRef((props, ref) => {
  const { children, type, icon, sublabel, ...rest } = props;
  return (
    <IGroup className="mb-3">
      <IGroup.Prepend>
        <IGroup.Text id="basic-addon1">{icon}</IGroup.Text>
      </IGroup.Prepend>
      <Form.Control type={type} {...rest} ref={ref} />
      {sublabel ? (
        <Form.Text className="text-light">{sublabel}</Form.Text>
      ) : null}
    </IGroup>
  );
});
InputGroup.defaultProps = { type: "text" };

export const Checkbox = forwardRef((props, ref) => {
  let r = (Math.random() + 1).toString(36).substring(7);
  const id = props.id ?? r;
  return (
    <div class="custom-control custom-checkbox position-relative">
      <input
        type="checkbox"
        class="custom-control-input"
        name={props.name}
        id={id}
        {...props}
      />
      <label class="custom-control-label f-14 text-muted" htmlFor={id}>
        {props.label}
      </label>
    </div>
  );
});
export const Radio = forwardRef((props, ref) => {
  let r = (Math.random() + 1).toString(36).substring(7);
  const id = props.id ?? r;
  return (
    <div class="custom-control custom-radio position-relative">
      <input
        type="radio"
        id={id}
        name={props.name}
        class="custom-control-input"
        {...props}
      />
      <label class="custom-control-label f-14 text-muted" htmlFor={id}>
        {props.label}
      </label>
    </div>
  );
});
export const Switch = forwardRef((props, ref) => {
  let r = (Math.random() + 1).toString(36).substring(7);
  const id = props.id ?? r;
  return (
    <div class="custom-control custom-switch">
      <input type="checkbox" class="custom-control-input" id={id} {...props} />
      <label class="custom-control-label f-14 text-muted" htmlFor={id}>
        {props.label}
      </label>
    </div>
  );
});

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <a
    href=""
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
    &#x25bc;
  </a>
));
export const Select = forwardRef((props, ref) => {
  return <Form.Select {...props}>{props.children}</Form.Select>;
  //   return (
  //     <>
  //       <select {...props} className="d-none">
  //         <option value={props.value}>{props.value}</option>
  //       </select>
  //       <Dropdown>
  //         <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
  //           {props.value || props.placeholder}
  //         </Dropdown.Toggle>

  //         <Dropdown.Menu as={CustomMenu}>
  //           <Dropdown.Item eventKey="1">Red</Dropdown.Item>
  //           <Dropdown.Item eventKey="2">Blue</Dropdown.Item>
  //           <Dropdown.Item eventKey="3"> Orange</Dropdown.Item>
  //           <Dropdown.Item eventKey="1">Red-Orange</Dropdown.Item>
  //         </Dropdown.Menu>
  //       </Dropdown>
  //     </>
  //   );
});
