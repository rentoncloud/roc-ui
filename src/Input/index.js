import Dropdown from "./Dropdown.js";
import RangePicker from "./RangePicker.js";
import {
  Text,
  Email,
  Password,
  Number,
  TextArea,
  Radio,
  Checkbox,
  Switch,
  Select,
  InputGroup,
} from "./Input.js";

const Input = {
  Text,
  Email,
  Password,
  Number,
  TextArea,
  RangePicker,
  Dropdown,
  Radio,
  Checkbox,
  Switch,
  Select,
  InputGroup,
};

export default Input;
