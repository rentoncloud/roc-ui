import React from "react";
import { Slider } from "antd";
import { Number } from "./Input";

export default function RangePicker(props) {
  return (
    <div className="">
      <Slider
        style={{ maxWidth: "90%", margin: "15px auto", ...props.style }}
        tooltipVisible={false}
        {...props}
      />
      <div className="row justify-content-between mt-4 mx-0">
        <div className="col px-0" style={{ maxWidth: "50px" }}>
          <Number className="text-center px-0" value={props.minValue} min="0" />
        </div>
        <div className="col px-0" style={{ maxWidth: "50px" }}>
          <Number className="text-center px-0" value={props.maxValue} />
        </div>
      </div>
    </div>
  );
}
