import Input from "./Input/index"
import Text from "./Text/index"
import Utils from "./Utils/index"
import { Button } from "react-bootstrap"

const returnLibrary = () => {
    return {
        Input,
        Text,
        Utils,
        Button
    }
}
export default returnLibrary()