import React from "react";

const H1 = ({ children, ...props }) => <h1 {...props}>{children}</h1>;
H1.defaultProps = { className: "roc-txt" };
const H2 = ({ children, ...props }) => <h2 {...props}>{children}</h2>;
H2.defaultProps = { className: "roc-txt" };
const H3 = ({ children, ...props }) => <h3 {...props}>{children}</h3>;
H3.defaultProps = { className: "roc-txt" };
const H4 = ({ children, ...props }) => <h4 {...props}>{children}</h4>;
H4.defaultProps = { className: "roc-txt" };
const H5 = ({ children, ...props }) => <h5 {...props}>{children}</h5>;
H5.defaultProps = { className: "roc-txt" };
const H6 = ({ children, ...props }) => <h6 {...props}>{children}</h6>;
H6.defaultProps = { className: "roc-txt" };
const P = ({ children, ...props }) => (
  <p style={{ fontSize: 16 }} {...props}>
    {children}
  </p>
);
const P2 = ({ children, ...props }) => (
  <p style={{ fontSize: 14 }} {...props}>
    {children}
  </p>
);
const P3 = ({ children, ...props }) => (
  <p style={{ fontSize: 12 }} {...props}>
    {children}
  </p>
);
const UL = ({ children, ...props }) => <ul {...props}>{children}</ul>;
const OL = ({ children, ...props }) => <ul {...props}>{children}</ul>;
const LI = ({ children, ...props }) => <li {...props}>{children}</li>;

const Text = {
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
  P,
  P2,
  P3,
  UL,
  OL,
  LI,
};
export default Text;
