import babel from "@rollup/plugin-babel";
import sourcemaps from "rollup-plugin-sourcemaps";

// the entry point for the library
const input = "src/index.js";

//
var MODE = [
  {
    fomart: "cjs",
  },
  {
    fomart: "esm",
  },
  {
    fomart: "umd",
  },
];

var config = [];

MODE.map((m) => {
  var conf = {
    input: input,
    output: {
      name: "roc",
      file: `dist/index.${m.fomart}.js`,
      format: m.fomart,
      exports: "auto",
      globals: {
        react: "React",
        "react-bootstrap": "react-bootstrap",
        antd: "antd",
      },
    },
    external: ["react", /@babel\/runtime/],
    plugins: [
      babel({
        exclude: "node_modules/**",
        plugins: ["@babel/transform-runtime"],
        babelHelpers: "runtime",
      }),
      sourcemaps(),
    ],
  };
  config.push(conf);
});

export default [...config];
