import { InputGroup as InputGroup$1, Form, Dropdown, Button } from 'react-bootstrap';
import _extends from '@babel/runtime/helpers/extends';
import _defineProperty from '@babel/runtime/helpers/defineProperty';
import React, { forwardRef } from 'react';
import { Slider } from 'antd';
import _objectWithoutProperties from '@babel/runtime/helpers/objectWithoutProperties';

var _excluded$1 = ["name", "label", "sublabel", "type"],
    _excluded2$1 = ["children", "type", "icon", "sublabel"];
// import RangePicker from "./RangePicker.js";

var InputField = /*#__PURE__*/forwardRef(function (_ref, ref) {
  var _props$id;

  var name = _ref.name,
      label = _ref.label,
      sublabel = _ref.sublabel,
      type = _ref.type,
      props = _objectWithoutProperties(_ref, _excluded$1);

  var r = (Math.random() + 1).toString(36).substring(7);
  var id = (_props$id = props.id) !== null && _props$id !== void 0 ? _props$id : r;
  return /*#__PURE__*/React.createElement(Form.Group, {
    className: "form-group",
    controlId: id !== null && id !== void 0 ? id : ""
  }, label ? /*#__PURE__*/React.createElement(Form.Label, null, label) : null, /*#__PURE__*/React.createElement(Form.Control, _extends({
    type: type
  }, props, {
    ref: ref,
    name: name
  })), sublabel ? /*#__PURE__*/React.createElement(Form.Text, {
    className: "text-light"
  }, sublabel) : null);
});
var Text$1 = /*#__PURE__*/forwardRef(function (props, ref) {
  return /*#__PURE__*/React.createElement(InputField, _extends({
    type: "text"
  }, props, {
    ref: ref
  }));
});
var Email = /*#__PURE__*/forwardRef(function (props, ref) {
  return /*#__PURE__*/React.createElement(InputField, _extends({
    type: "email"
  }, props, {
    ref: ref
  }));
});
var Password = /*#__PURE__*/forwardRef(function (props, ref) {
  return /*#__PURE__*/React.createElement(InputField, _extends({
    type: "password"
  }, props, {
    ref: ref
  }));
});
var Number = /*#__PURE__*/forwardRef(function (props, ref) {
  return /*#__PURE__*/React.createElement(InputField, _extends({
    type: "number"
  }, props, {
    ref: ref
  }));
});
var TextArea = /*#__PURE__*/forwardRef(function (props, ref) {
  return /*#__PURE__*/React.createElement(InputField, _extends({
    as: "textarea"
  }, props, {
    ref: ref
  }));
});
TextArea.defaultProps = {
  rows: 3
};
var InputGroup = /*#__PURE__*/forwardRef(function (props, ref) {
  props.children;
      var type = props.type,
      icon = props.icon,
      sublabel = props.sublabel,
      rest = _objectWithoutProperties(props, _excluded2$1);

  return /*#__PURE__*/React.createElement(InputGroup$1, {
    className: "mb-3"
  }, /*#__PURE__*/React.createElement(InputGroup$1.Prepend, null, /*#__PURE__*/React.createElement(InputGroup$1.Text, {
    id: "basic-addon1"
  }, icon)), /*#__PURE__*/React.createElement(Form.Control, _extends({
    type: type
  }, rest, {
    ref: ref
  })), sublabel ? /*#__PURE__*/React.createElement(Form.Text, {
    className: "text-light"
  }, sublabel) : null);
});
InputGroup.defaultProps = {
  type: "text"
};
var Checkbox = /*#__PURE__*/forwardRef(function (props, ref) {
  var _props$id2;

  var r = (Math.random() + 1).toString(36).substring(7);
  var id = (_props$id2 = props.id) !== null && _props$id2 !== void 0 ? _props$id2 : r;
  return /*#__PURE__*/React.createElement("div", {
    "class": "custom-control custom-checkbox position-relative"
  }, /*#__PURE__*/React.createElement("input", _extends({
    type: "checkbox",
    "class": "custom-control-input",
    name: props.name,
    id: id
  }, props)), /*#__PURE__*/React.createElement("label", {
    "class": "custom-control-label f-14 text-muted",
    htmlFor: id
  }, props.label));
});
var Radio = /*#__PURE__*/forwardRef(function (props, ref) {
  var _props$id3;

  var r = (Math.random() + 1).toString(36).substring(7);
  var id = (_props$id3 = props.id) !== null && _props$id3 !== void 0 ? _props$id3 : r;
  return /*#__PURE__*/React.createElement("div", {
    "class": "custom-control custom-radio position-relative"
  }, /*#__PURE__*/React.createElement("input", _extends({
    type: "radio",
    id: id,
    name: props.name,
    "class": "custom-control-input"
  }, props)), /*#__PURE__*/React.createElement("label", {
    "class": "custom-control-label f-14 text-muted",
    htmlFor: id
  }, props.label));
});
var Switch = /*#__PURE__*/forwardRef(function (props, ref) {
  var _props$id4;

  var r = (Math.random() + 1).toString(36).substring(7);
  var id = (_props$id4 = props.id) !== null && _props$id4 !== void 0 ? _props$id4 : r;
  return /*#__PURE__*/React.createElement("div", {
    "class": "custom-control custom-switch"
  }, /*#__PURE__*/React.createElement("input", _extends({
    type: "checkbox",
    "class": "custom-control-input",
    id: id
  }, props)), /*#__PURE__*/React.createElement("label", {
    "class": "custom-control-label f-14 text-muted",
    htmlFor: id
  }, props.label));
});
var Select = /*#__PURE__*/forwardRef(function (props, ref) {
  return /*#__PURE__*/React.createElement(Form.Select, props, props.children); //   return (
  //     <>
  //       <select {...props} className="d-none">
  //         <option value={props.value}>{props.value}</option>
  //       </select>
  //       <Dropdown>
  //         <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
  //           {props.value || props.placeholder}
  //         </Dropdown.Toggle>
  //         <Dropdown.Menu as={CustomMenu}>
  //           <Dropdown.Item eventKey="1">Red</Dropdown.Item>
  //           <Dropdown.Item eventKey="2">Blue</Dropdown.Item>
  //           <Dropdown.Item eventKey="3"> Orange</Dropdown.Item>
  //           <Dropdown.Item eventKey="1">Red-Orange</Dropdown.Item>
  //         </Dropdown.Menu>
  //       </Dropdown>
  //     </>
  //   );
});

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function RangePicker(props) {
  return /*#__PURE__*/React.createElement("div", {
    className: ""
  }, /*#__PURE__*/React.createElement(Slider, _extends({
    style: _objectSpread({
      maxWidth: "90%",
      margin: "15px auto"
    }, props.style),
    tooltipVisible: false
  }, props)), /*#__PURE__*/React.createElement("div", {
    className: "row justify-content-between mt-4 mx-0"
  }, /*#__PURE__*/React.createElement("div", {
    className: "col px-0",
    style: {
      maxWidth: "50px"
    }
  }, /*#__PURE__*/React.createElement(Number, {
    className: "text-center px-0",
    value: props.minValue,
    min: "0"
  })), /*#__PURE__*/React.createElement("div", {
    className: "col px-0",
    style: {
      maxWidth: "50px"
    }
  }, /*#__PURE__*/React.createElement(Number, {
    className: "text-center px-0",
    value: props.maxValue
  }))));
}

var Input = {
  Text: Text$1,
  Email: Email,
  Password: Password,
  Number: Number,
  TextArea: TextArea,
  RangePicker: RangePicker,
  Dropdown: Dropdown,
  Radio: Radio,
  Checkbox: Checkbox,
  Switch: Switch,
  Select: Select,
  InputGroup: InputGroup
};

var _excluded = ["children"],
    _excluded2 = ["children"],
    _excluded3 = ["children"],
    _excluded4 = ["children"],
    _excluded5 = ["children"],
    _excluded6 = ["children"],
    _excluded7 = ["children"],
    _excluded8 = ["children"],
    _excluded9 = ["children"],
    _excluded10 = ["children"],
    _excluded11 = ["children"],
    _excluded12 = ["children"];

var H1 = function H1(_ref) {
  var children = _ref.children,
      props = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/React.createElement("h1", props, children);
};

H1.defaultProps = {
  className: "roc-txt"
};

var H2 = function H2(_ref2) {
  var children = _ref2.children,
      props = _objectWithoutProperties(_ref2, _excluded2);

  return /*#__PURE__*/React.createElement("h2", props, children);
};

H2.defaultProps = {
  className: "roc-txt"
};

var H3 = function H3(_ref3) {
  var children = _ref3.children,
      props = _objectWithoutProperties(_ref3, _excluded3);

  return /*#__PURE__*/React.createElement("h3", props, children);
};

H3.defaultProps = {
  className: "roc-txt"
};

var H4 = function H4(_ref4) {
  var children = _ref4.children,
      props = _objectWithoutProperties(_ref4, _excluded4);

  return /*#__PURE__*/React.createElement("h4", props, children);
};

H4.defaultProps = {
  className: "roc-txt"
};

var H5 = function H5(_ref5) {
  var children = _ref5.children,
      props = _objectWithoutProperties(_ref5, _excluded5);

  return /*#__PURE__*/React.createElement("h5", props, children);
};

H5.defaultProps = {
  className: "roc-txt"
};

var H6 = function H6(_ref6) {
  var children = _ref6.children,
      props = _objectWithoutProperties(_ref6, _excluded6);

  return /*#__PURE__*/React.createElement("h6", props, children);
};

H6.defaultProps = {
  className: "roc-txt"
};

var P = function P(_ref7) {
  var children = _ref7.children,
      props = _objectWithoutProperties(_ref7, _excluded7);

  return /*#__PURE__*/React.createElement("p", _extends({
    style: {
      fontSize: 16
    }
  }, props), children);
};

var P2 = function P2(_ref8) {
  var children = _ref8.children,
      props = _objectWithoutProperties(_ref8, _excluded8);

  return /*#__PURE__*/React.createElement("p", _extends({
    style: {
      fontSize: 14
    }
  }, props), children);
};

var P3 = function P3(_ref9) {
  var children = _ref9.children,
      props = _objectWithoutProperties(_ref9, _excluded9);

  return /*#__PURE__*/React.createElement("p", _extends({
    style: {
      fontSize: 12
    }
  }, props), children);
};

var UL = function UL(_ref10) {
  var children = _ref10.children,
      props = _objectWithoutProperties(_ref10, _excluded10);

  return /*#__PURE__*/React.createElement("ul", props, children);
};

var OL = function OL(_ref11) {
  var children = _ref11.children,
      props = _objectWithoutProperties(_ref11, _excluded11);

  return /*#__PURE__*/React.createElement("ul", props, children);
};

var LI = function LI(_ref12) {
  var children = _ref12.children,
      props = _objectWithoutProperties(_ref12, _excluded12);

  return /*#__PURE__*/React.createElement("li", props, children);
};

var Text = {
  H1: H1,
  H2: H2,
  H3: H3,
  H4: H4,
  H5: H5,
  H6: H6,
  P: P,
  P2: P2,
  P3: P3,
  UL: UL,
  OL: OL,
  LI: LI
};

function Utils() {
  return {
    test: "test"
  };
}

var returnLibrary = function returnLibrary() {
  return {
    Input: Input,
    Text: Text,
    Utils: Utils,
    Button: Button
  };
};

var index = returnLibrary();

export { index as default };
